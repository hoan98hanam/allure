package page;

import basepage.BasePage;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    private final By userNameTextBox = By.id("loginusername");
    private final By passwordTextBox = By.id("loginpassword");
    private final By loginInButton = By.id("login2");
    private final By verifyLogin = By.id("nameofuser");
    private final By signInButton = By.xpath("//button[@onclick=\"logIn()\"]");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void setEmail(String username) {
        sendKeys(userNameTextBox, username);
    }

    public void clickButtonLogin() {
        click(loginInButton);
    }

    public void setPassword(String password) {
        sendKeys(passwordTextBox, password);
    }

    public void clickSignIn() {
        click(signInButton);
    }

    @Step("Login method")
    public void login(String username, String password) {
        clickButtonLogin();
        setEmail(username);
        setPassword(password);
        clickSignIn();
    }

    @Step("verify login after click login button")
    public String verifyLogin() {
        return getText(verifyLogin);
    }
}
